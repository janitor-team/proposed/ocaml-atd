ocaml-atd (2.2.1-1) unstable; urgency=medium

  * New upstream release
  * Add myself to Uploaders and remove Hendrik
  * Bump debhelper compat level to 13

 -- Stéphane Glondu <glondu@debian.org>  Thu, 20 Aug 2020 13:33:28 +0200

ocaml-atd (2.1.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update Homepage and debian/watch
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Fri, 31 Jan 2020 14:14:00 +0100

ocaml-atd (2.0.0-3) unstable; urgency=medium

  * Team upload
  * Fix call to "jbuilder install" (Closes: #917733)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 09 Jan 2019 16:49:20 +0100

ocaml-atd (2.0.0-2) unstable; urgency=medium

  * Team upload
  * Remove a test that fails on i386 (Closes: #901823)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 08 Dec 2018 10:39:31 +0100

ocaml-atd (2.0.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - add default-jdk-headless to Build-Depends
  * Update Homepage
  * Update debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Mon, 25 Jun 2018 11:04:00 +0200

ocaml-atd (1.12.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.12.0
    - switch to jbuilder
    - latex and html docs cannot be generated anymore
    - drop patch to toplevel Makefile
  * The new upstream release now includes src:atdgen and its binary packages
    - added binary packages libatdgen-ocaml-dev and libatdgen-ocaml
    - added build dependency on libbiniou-ocaml-dev and libyojson-ocaml-dev
  * Bump debhelper compat to 11
  * Remove trailing whitespaces
  * Bump Standards-Version to 4.1.4 (no changes required)
  * Switch Vcs-* fields to salsa
  * debian/copyright: add missing bit from license
  * debian/rules: use dh_missing --list-missing instead of
    dh_install --fail-missing
  * add autopkgtest

 -- Johannes Schauer <josch@debian.org>  Wed, 23 May 2018 08:40:27 +0200

ocaml-atd (1.1.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Wed, 06 Aug 2014 11:03:09 +0200

ocaml-atd (1.1.1-2) unstable; urgency=medium

  * Team upload
  * Fix package build on bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Fri, 31 Jan 2014 17:30:19 +0100

ocaml-atd (1.1.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Stéphane Glondu <glondu@debian.org>  Wed, 29 Jan 2014 14:33:16 +0100

ocaml-atd (1.0.3-1) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

  [ Hendrik Tews ]
  * fix watch file and hompage
  * new upstream version
  * add myself as uploader
  * bump debhelper compat level and standards version
  * update package description
  * update copyright
  * fix patches
    - delete old patch
    - add makefile.patch for byte compilation and installation of some ml files
    - add lintian-allows-to.patch to fix lintian allows warning
    - add build-manual-without-install.patch to build manual/atd-manual.html
      without prior installation of the atd library
  * update rules and other debhelper files
  * fix build dependencies
  * install pdf version of manual
  * enable tests
  * rewrite/improve man page

 -- Hendrik Tews <hendrik@askra.de>  Thu, 06 Jun 2013 10:21:07 +0200

ocaml-atd (1.0.1-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.2

 -- Stéphane Glondu <glondu@debian.org>  Mon, 11 Jul 2011 00:01:50 +0200

ocaml-atd (0.9.2-2) unstable; urgency=low

  * Add doc-base for ATD manual

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 04 Dec 2010 23:43:36 +0100

ocaml-atd (0.9.2-1) unstable; urgency=low

  * Initial release. (Closes: #605670)

 -- Sylvain Le Gall <gildor@debian.org>  Thu, 02 Dec 2010 22:22:54 +0100
